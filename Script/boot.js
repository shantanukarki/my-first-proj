window.onscroll = function () {
  myFunction();
};
var nav = document.getElementById("stick");
var sticky = nav.offsetTop;
function myFunction() {
  if (window.pageYOffset >= sticky) {
    nav.classList.add("sticky");
    nav.style.position="fixed";
  } else {
    nav.classList.remove("sticky");
    nav.style.position="relative";
  }
}
function opensearch() {
  document.getElementById("mysearch").style.visibility = "visible";
}
function closesearch() {
  document.getElementById("mysearch").style.visibility = "hidden";
}
function getmenu() {
  var x = document.getElementById("mob-menu");
  if (x.style.display == "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
$(".topslide").slick({});
$(".slide").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  dots: true,
  arrows: false,
});
/* About Us JS */
var tooltips = document.querySelectorAll('.timeText');
window.onmousemove = function (e) {
  var x = (e.clientX) + 'px',
      y = (e.clientY) + 'px';
  for (var i = 0; i < tooltips.length; i++) {
      tooltips[i].style.top = y;
      tooltips[i].style.left = x;
  }
};
var x = document.getElementById("headtext");
var y = document.getElementById("moretxt");
var z = document.getElementById("lesstxt");
function opentext() {
  x.style.display = "block";
  y.style.display = "none";
  z.style.display = "block";
}
function closetxt() {
  x.style.display = "none";
  y.style.display = "block";
  z.style.display = "none";
}
$(".slideshow").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  infinite: true,
  dots: false,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 3000,
});
$(document).ready(function(){
  $('.quotesSlide').slick({
  })
})
/* Course JS */
$(document).ready(function(){
  $('#open1').click(function(){
    $('#first').slideToggle("slow");
  })
});
$(document).ready(function(){
  $('#open2').click(function(){
    $('#second').slideToggle("slow");
  })
});
/*Gallery Js */
$(document).ready(function () {
  $(".gallerySlide-main").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    autoplay: true,
    autoplaySpeed: 5000,
    asNavFor: ".gallerySlide-nav",
  });
});
$(document).ready(function () {
  $(".gallerySlide-nav").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: ".gallerySlide-main",
    centerMode: true,
    focusOnSelect: true,
  });
});
/*Contact Js */
$(document).ready(function(){
  $("#opencont1").click(function(){
    $("#contactInfo1").slideDown("slow");
    $("#contactInfo2").slideUp("slow");
    $("#contactInfo3").slideUp("slow");
    $("#contactInfo4").slideUp("slow");
    $("#contactInfo5").slideUp("slow");
  });
});
$(document).ready(function(){
  $("#opencont2").click(function(){
    $("#contactInfo2").slideDown("slow");
    $("#contactInfo1").slideUp("slow");
    $("#contactInfo3").slideUp("slow");
    $("#contactInfo4").slideUp("slow");
    $("#contactInfo5").slideUp("slow");
  });
});
$(document).ready(function(){
  $("#opencont3").click(function(){
    $("#contactInfo3").slideDown("slow");
    $("#contactInfo1").slideUp("slow");
    $("#contactInfo2").slideUp("slow");
    $("#contactInfo4").slideUp("slow");
    $("#contactInfo5").slideUp("slow");
    
  });
});
$(document).ready(function(){
  $("#opencont4").click(function(){
    $("#contactInfo4").slideDown("slow");
    $("#contactInfo1").slideUp("slow");
    $("#contactInfo2").slideUp("slow");
    $("#contactInfo3").slideUp("slow");
    $("#contactInfo5").slideUp("slow");
  });
});
$(document).ready(function(){
  $("#opencont5").click(function(){
    $("#contactInfo5").slideDown("slow");
    $("#contactInfo1").slideUp("slow");
    $("#contactInfo2").slideUp("slow");
    $("#contactInfo3").slideUp("slow");
    $("#contactInfo4").slideUp("slow");
  });
});
/*Events Js */
$(document).ready(function () {
  $(".eventslider").slick({
    slidesToShow: 2,
    slidesToScroll: 1,
  });
});
var settings = {};
var events = [
  {
    Date: new Date(2021, 3, 7),
    Title: "For master's from 2pm to 5pm",
    Link: "#",
  },
  {
    Date: new Date(2021, 3, 18),
    Title: "For bachleor's from 2pm to 5pm",
    Link: "#",
  },
  { Date: new Date(2021, 3, 27), Title: "For +2 from 2pm to 5pm", Link: "#" },
];
var element = document.getElementById("caleandar");
caleandar(element, events, settings);
